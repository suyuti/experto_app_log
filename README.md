# Experto_App_Log

## Kapsam

Experto için geliştirilen uygulama kullanıldığı dönemde tüm işlemleri veritabanına kaydetmiştir. Burada görülen dosyalar veritabanından alınan kayıtların örneğidir.
Kayıtlar 18 Ekim 2021 tarihinde çalışmakta olan veritabanından alınmıştır.
Daha sonra bu veri tabanı silinmiş. Kayıtlara erişilememiştir. Kayıt logları geliştirilen yazılımın aktif olarak kullanıldığını göstermektedir.

ExpertoRapor.pdf dosyasında detaylar gösterilmektedir.

## Geliştirilen Yazılımın Kullanım Sayıları

Geliştirilen yazılım Experto şirketi personeli tarafından düzenli olarak kullanılmıştır. Aşağıdaki listede kullanım istatistikleri vardır.
Her işlemin Experto firmasından kimin tarafından ve ne zaman yapıldığı kayıt altındadır. 
Kullanım istatistikleri 18 Ekim 2021 tarihinde alınmıştır.


|Kategori|Adet|
|----------------|-------------|
| Toplam müşteri | 4.627 firma |
|Toplam kullanıcı|44 kişi|
|Toplam müşteri irtibat kişisi|7.402 kişi|
|Toplam toplantı sayısı|1.766 toplantı
|Toplam Görüşme kayıt sayısı|1.991 görüşme notu
|Toplam görev sayısı|11.397 görev
|Toplam ürün sayısı|27 ürün
|Toplam kesilen fatura adedi|101 fatura|
|Toplam yorum sayısı|57 adet|

## Yazılımın Kestiği EFaturalar

Experto için geliştirilen yazılım kullanılarak QNB bankacılık sistemi ile entegre şekilde eFaturalar kesilmiştir.
Kesilen faturalar 101 adettir.
Kesilen faturalara ilişkin detaylar fatura.json dosyasındadır. EFatura uniqueID'leri QNB bankası tarafındaki işlem logları ile takip edilebilir ve doğrulanabilir.

## Experto Sisteminde Tanımlı Kullanıcılar

Bir kullanıcının Experto yazılımını kullanabilmesi için Experto firmasının Microsoft Exchange hesabı ile giriş yapması gerekir. Kayıtlardaki kullanıcı bilgisi Microsoft’tan gelen kullanıcı hesabını ayırt edici değerdir. Kayıtlardaki kullanıcı numarası ve Microsoft kulalnıcı hesapları aynıdır. Aşağıdaki liste Experto firmasının Microsoft hesabından alınmıştır. Geliştirilen yazılımın aşağıdaki kullanıcılar tarafından kullanıldığı kayıtlardan görülebilir. 

|Kullanıcı Adı|Mail Adresi|Kullanıcı ID|
|-------------|-----------|------------|
Adem Gocmez | adem.gocmez@experto.com.tr | ef2894df-c938-4808-b767-763c3f26dddf
Admin Experto | admin@EXPERTOTR.onmicrosoft.com | cc9893e5-749b-4be8-a93c-1f54dd97d0ca
Alpaslan Birol | alpaslan.birol@experto.com.tr | 95fb6b30-e944-42ad-acce-ade94426eadd
Alper Engin | alper.engin@experto.com.tr | 0528874a-2ce2-400e-bb84-b0a245c257df
Anıl Tez | anil.tez@experto.com.tr | 9d487375-3803-437d-959d-8de8af696183
Asistan | asistan@experto.com.tr | 4f4126ea-fdd6-4069-8357-ba9c9e8fd666
Berkcan Terzioglu | berkcan.terzioglu@experto.com.tr | a1a65324-5dee-4cc1-9650-1b9cb906337d
Büşra Özge Hacıbayramoğlu | busra.hacibayramoglu@experto.com.tr | 17fec524-ee56-4676-8b42-cbdbefaf8914
Çağrı Akman | cagri.akman@experto.com.tr | a1b2d9cb-bcd5-45cd-8b59-022c078fb54b
Dilruba Sağlam Ülker | dilruba.saglam@experto.com.tr | effb9aa3-907e-47a3-8eff-4ea45bf59237
Dual | dual@experto.com.tr | 1acc0e1f-51ad-41a4-a464-0f079527e36e
Enes Özbey | enes.ozbey@experto.com.tr | df256ae7-42ee-450a-afbe-1fd83ba0698c
Ensar Üçer | ensar.ucer@experto.com.tr | 92683b4b-4954-4250-89f8-f60664df1859
Eray Tanlası | eray.tanlasi@experto.com.tr | 3b584b9a-1558-4f0e-8406-d90e55d170d9
Experto Eğitim | egitim@experto.com.tr | e209440b-c94d-4a20-87ef-b82309638b2d
ExpertoAPP | expertoapp@experto.com.tr | 5b487f55-8501-4b6f-9297-f025cd56e1e1
EYÜP BALDİŞ | eyup.baldis@experto.com.tr | a69f9e05-c57b-4c5b-a1c8-15a767777a50
Fatoş Genç | fatos.genc@experto.com.tr | a3b16830-196c-4c09-a375-aff78d1395d0
Gizem Akdut | gizem.akdut@experto.com.tr | 8905612e-3967-494e-81c0-c883d0b5234b
Gizem Yeşiltaş | gizem.yesiltas@experto.com.tr | c9cddaa2-1d2f-4ef5-9eb6-70afd8c8ca5c
Hasan Birol | hasan.birol@experto.com.tr | 8ef44284-85ee-47e9-a3c6-fb20fd7c2412
Hilal Gülsever | hilal.gulsever@experto.com.tr | 4df6729e-27c4-4838-a667-09bf8c347246
Hilal Yılmaz | hilal.yilmaz@experto.com.tr | 3fa1c6da-9f42-44e8-9a0c-9bc09c904002
Hülya Hacıbayramoğlu | hulya.hacibayramoglu@experto.com.tr | f06f40e0-906e-4a56-b524-870c54c4f58d
Jülide Birol | julide.birol@experto.com.tr | 092a66e6-2a24-4b54-aea2-d1658185da35
Kübra İstemez | kubra.istemez@experto.com.tr | 49ebc177-6553-420b-988b-327bf84003ae
Mehmet Dindar | mehmet.dindar@experto.com.tr | 1c88de8b-4d0d-4f59-817c-61f2b76438f0
Merve Albayrak Birol | merve.albayrak@experto.com.tr | 9bae6532-6331-4bab-bf08-14d15e0599d8
Merve Çelik | merve.celik@experto.com.tr | b44cdfd1-b318-4f6e-b034-2b0b8519f87f
Münire Eliboloğlu | munire.elibologlu@experto.com.tr | 75f6e9c6-e167-4a42-a489-15d1442fff67
Mustafa Berbercioglu | mustafa.berbercioglu@experto.com.tr | acdc9873-c629-4e6e-bbeb-40ab96c13ff9
Nihat Demirtaş | nihat.demirtas@experto.com.tr | cda6ec2e-b471-40b2-a81a-d73e1b7316ec
Özgüç Aydın | ozguc.aydin@experto.com.tr | 3423e252-2040-4f8a-ae5d-8f7dbe8b3512
Özgül Yıldız | ozgul.yildiz@experto.com.tr | fcbb7ef0-737f-4f04-848f-0edaa507f327
Özgür Palantöken | ozgur.palantoken@experto.com.tr | ebc22895-6058-406f-b934-a4ab8fa7f340
Perihan Dündar | perihan.dundar@experto.com.tr | 0c2ee695-3a6b-4922-90d5-37a167438c53
Pınar Şenli | pinar.senli@experto.com.tr | 82fae308-0952-43ef-9f6f-6afa5af52417
REŞAT SOFUOĞLU | resat.sofuoglu@experto.com.tr | 209d36d1-ba03-4596-a797-f4f7943e48b8
Selçuk Erzaim | selcuk.erzaim@experto.com.tr | 7bbb30bd-93de-4a3a-ac8c-752fe74dc3e8
Semih Atik | semih.atik@experto.com.tr | bad6b6c6-4bdc-465d-9fea-59c29d47f701
Sertaç Hikmet Atalay | sertac.atalay@experto.com.tr | c28b332a-8581-4c6f-839d-a66919d4e8d0
Taha Ülker | taha.ulker@experto.com.tr | d1b972e8-48ef-40bf-927c-18007ecf85d2
Tayibe Sönmez | tayibe.sonmez@experto.com.tr | d3065b28-9038-45f9-9562-21a43aa786e1
Tuğçe Çorbacı | tugce.corbaci@experto.com.tr | ed1178d7-34f2-4f23-a2c2-ed549c7af18d
turgut erkaynak | turgut.erkaynak@experto.com.tr | 4e40201e-17ed-4d86-91f5-a3cc812d3445
Yasin Karapınar | catalca@experto.com.tr | aba30557-9fcf-442f-8921-4e857c17d8f3
YİĞİT İNCE | yigit.ince@experto.com.tr | a638ed86-90e6-49aa-931b-2c08cfe60042

